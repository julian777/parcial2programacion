clear all

Monedas=[1,2,5]; %Posibles denominaciones
cant=input('Ingrese la cantidad a cambiar'); %Ingreso de cantidad de inter�s


n=length(Monedas); %Asigna la longitud del vector a otra variable
vectormonedas=zeros(1,n);  %El vector monedas estar�a compuesto por 1 fila y n columnas (Inicializado en ceros)  
for i=n:-1:1               %Se decrementa en el vector de 3 posiciones
    while Monedas(i)<=cant  %Si monedas en la posici�n indicada es menor a la cantidad ingresada a cambiar;     
                 vectormonedas(i)=1+vectormonedas(i); %Se agrega 1 en la posici�n indicada.
                 cant=cant-Monedas(i);  %Se resta la cantidad de la posici�n en la cantidad a cambiar.
    end
end
 %vectormonedas    %Devuelve la soluci�n

disp('El n�mero de monedas de 5 es: ')
vectormonedas(3)
disp('Lo cual suma: ')
vectormonedas(3)*5
disp('El n�mero de monedas de 3 es: ')
vectormonedas(2)
disp('Lo cual suma: ')
vectormonedas(2)*2
disp('El n�mero de monedas de 1 es: ')
vectormonedas(1)
disp('Lo cual suma: ')
vectormonedas(1)
